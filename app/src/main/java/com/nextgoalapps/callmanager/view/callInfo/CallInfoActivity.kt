package com.nextgoalapps.callmanager.view.callInfo

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.nextgoalapps.callmanager.App
import com.nextgoalapps.callmanager.R
import com.nextgoalapps.callmanager.di.components.screens.DaggerCallInfoComponent
import com.nextgoalapps.callmanager.presenter.callInfo.CallInfoPresenter
import com.nextgoalapps.callmanager.utils.Utils
import com.nextgoalapps.callmanager.view.BaseActivity
import com.nextgoalapps.callmanager.view.settings.SettingsActivity
import java.text.DecimalFormat
import javax.inject.Inject

class CallInfoActivity : BaseActivity(), CallInfoView {

    @BindView(R.id.toolbar) lateinit var toolbar: Toolbar
    @BindView(R.id.projects) lateinit var projects: EditText
    @BindView(R.id.projects_list) lateinit var projectsList: ListView
    @BindView(R.id.number) lateinit var numberText: TextView
    @BindView(R.id.duration) lateinit var duration: TextView
    @BindView(R.id.date_text) lateinit var dateText: TextView
    @BindView(R.id.never_save_button) lateinit var neverSaveButton: Button

    private lateinit var unbinder: Unbinder

    @Inject lateinit var mCallInfoPresenter: CallInfoPresenter

    @OnClick(R.id.cancel_button)
    fun cancelSaving() {
        reset()
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    @OnClick(R.id.never_save_button)
    fun neverSave() {
        mCallInfoPresenter.addDisabledCaller()
        reset()
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    @OnClick(R.id.refresh_button)
    fun refreshProjects(){
        mCallInfoPresenter.updateProjects()
    }

    private fun reset() {
        mCallInfoPresenter.reset()
        numberText.text = ""
        duration.text = ""
    }

    @OnClick(R.id.decrement_time_button)
    fun decrementDuration() {
        mCallInfoPresenter.decrementDuration()
        setCallDuration()
    }

    @OnClick(R.id.increment_time_button)
    fun incrementDuration() {
        mCallInfoPresenter.incrementDuration()
        setCallDuration()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_call_info)
        unbinder = ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        DaggerCallInfoComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this)

        mCallInfoPresenter.attachView(this)
        mCallInfoPresenter.init()

        dateText.text = Utils.getCurrentDate()

        val extras = intent.extras

        if (extras != null) {
            mCallInfoPresenter.setNumber(extras.getString("Number"))
            if (mCallInfoPresenter.isCallerDisabled()) finish()
            numberText.text = mCallInfoPresenter.numberPresentation
            val difference = extras.getLong("Duration")
            setCallDuration(difference)
        } else {
            neverSaveButton.visibility = View.INVISIBLE
        }

        projects.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                mCallInfoPresenter.search(charSequence.toString())
                val arrayAdapter = ArrayAdapter(this@CallInfoActivity,
                        android.R.layout.simple_list_item_1,
                        mCallInfoPresenter.projectsPresentation)
                projectsList.adapter = arrayAdapter
            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }

    private fun setCallDuration(durationValue: Long? = null) {
        val res = if (durationValue != null) {
            mCallInfoPresenter.getCallDuration(durationValue)
        } else {
            mCallInfoPresenter.getCallDuration()
        }
        val result = if (res == -1.0) {
            ""
        } else {
            val differenceInSeconds = res / DateUtils.SECOND_IN_MILLIS
            val formatted = DateUtils.formatElapsedTime(differenceInSeconds.toLong())
            val df = DecimalFormat("#.#")
            df.format(res) + " h (" + formatted + ")"
        }
        duration.text = result
    }


    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            101 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mCallInfoPresenter.startService()
                mCallInfoPresenter.updateProjects()
            } else {
                showMessage("We can't handle calls without the permission.")
            }
            102 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                numberText.text = mCallInfoPresenter.numberPresentation
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun getViewActivity(): Activity {
        return this
    }

    override fun onProjectsLoaded() {
        val arrayAdapter = ArrayAdapter(this,
                android.R.layout.simple_list_item_1,
                mCallInfoPresenter.projectsPresentation)
        projectsList.adapter = arrayAdapter
        projectsList.setOnItemClickListener { adapterView, view, i, l ->
            if (mCallInfoPresenter.getCallDuration() != -1.0) {
                mCallInfoPresenter.saveCallDuration(i)
            } else {
                showMessage("You should make a call first.")
            }
        }
    }

    override fun onProjectsLoadError() {
        showMessage("Can't load projects right now. Try again later.")
    }

    override fun onCallSaved() {
        Toast.makeText(applicationContext, "The time has been saved to the project.", Toast.LENGTH_SHORT).show()
        // showMessage("The time has been saved to the project.");
        reset()
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }

    override fun onCallSaveError() {
        showMessage("Can't save the time right now. Try again.")
    }

    private fun showMessage(text: String) {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(text)
                .setCancelable(true)
                .setNeutralButton("Ok") { dialogInterface, i -> dialogInterface.dismiss() }
                .create()
                .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.call_info_action_buttons, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
//            R.id.action_refresh -> mCallInfoPresenter.updateProjects()
            R.id.action_settings -> startActivity(Intent(this, SettingsActivity::class.java))
        }
        return true
    }
}
