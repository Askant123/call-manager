package com.nextgoalapps.callmanager.view;


import android.app.Activity;

public interface BaseView {
    void showLoadProgress(String... message);
    void hideLoadProgress();

    Activity getViewActivity();
}
