package com.nextgoalapps.callmanager.view.settings

import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Spinner
import android.widget.Switch
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.baoyz.swipemenulistview.SwipeMenu
import com.baoyz.swipemenulistview.SwipeMenuItem
import com.baoyz.swipemenulistview.SwipeMenuListView
import com.nextgoalapps.callmanager.App
import com.nextgoalapps.callmanager.R
import com.nextgoalapps.callmanager.di.components.screens.DaggerSettingsComponent
import com.nextgoalapps.callmanager.presenter.settings.SettingsPresenter
import com.nextgoalapps.callmanager.utils.Utils
import com.nextgoalapps.callmanager.view.BaseActivity
import com.nextgoalapps.callmanager.view.settings.nested.DisabledCallsAdapter
import javax.inject.Inject

class SettingsActivity : BaseActivity(), SettingsView {

    private lateinit var unbinder: Unbinder
    @Inject lateinit var mSettingsPresenter: SettingsPresenter

    @BindView(R.id.disabled_list) lateinit var disabledList: SwipeMenuListView
    @BindView(R.id.language_spinner) lateinit var languageSpinner: Spinner
//    @BindView(R.id.disable_caller_switch) lateinit var disableCallerSwitch: Switch

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        unbinder = ButterKnife.bind(this)

        DaggerSettingsComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this)

        mSettingsPresenter.attachView(this)

        val creator = { menu: SwipeMenu ->
            val deleteItem = SwipeMenuItem(applicationContext)
            deleteItem.background = ColorDrawable(Color.RED)
            deleteItem.width = Utils.dpToPx(100, this)
            deleteItem.setIcon(R.mipmap.ic_delete)
//            deleteItem.title = "Delete"
//            // set item title fontsize
//            deleteItem.titleSize = 18
//            // set item title font color
//            deleteItem.titleColor = Color.WHITE
            // add to menu
            menu.addMenuItem(deleteItem)
        }

        disabledList.setMenuCreator(creator)
        disabledList.adapter = DisabledCallsAdapter(mSettingsPresenter.getDisabledCallers())
        Utils.setListViewHeightBasedOnChildren(disabledList)

        disabledList.setOnMenuItemClickListener({ position, menu, index ->
            mSettingsPresenter.removeCaller(index)
            disabledList.adapter = DisabledCallsAdapter(mSettingsPresenter.getDisabledCallers())
            Utils.setListViewHeightBasedOnChildren(disabledList)
            false
        })

        val languages = listOf("English", "Estonian", "Latvian", "Russian")

        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, languages)

        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        languageSpinner.adapter = arrayAdapter

//        disableCallerSwitch.isChecked = mSettingsPresenter.getIsDisabledCallersActive()
//
//        disableCallerSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
//            run {
//                mSettingsPresenter.setIsDisabledCallersActive(isChecked)
//            }
//        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            102 -> if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                disabledList.adapter = DisabledCallsAdapter(mSettingsPresenter.getDisabledCallers())
                Utils.setListViewHeightBasedOnChildren(disabledList)
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

}
