package com.nextgoalapps.callmanager.view.login

import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.nextgoalapps.callmanager.App
import com.nextgoalapps.callmanager.R
import com.nextgoalapps.callmanager.di.components.screens.DaggerLoginComponent
import com.nextgoalapps.callmanager.presenter.login.LoginPresenter
import com.nextgoalapps.callmanager.view.BaseActivity
import com.nextgoalapps.callmanager.view.callInfo.CallInfoActivity
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginView {

    private lateinit var unbinder: Unbinder

    @Inject
    internal lateinit var mLoginPresenter: LoginPresenter

    @BindView(R.id.email)
    lateinit var loginTextView: TextView

    @BindView(R.id.password)
    lateinit var passwordTextView: TextView

    @OnClick(R.id.login_button)
    fun login() {
        mLoginPresenter.login(loginTextView.text.toString(),
                passwordTextView.text.toString())
    }

    @BindView(R.id.sign_up_button)
    lateinit var signUp: TextView

    @BindView(R.id.agreement)
    lateinit var agreement: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        unbinder = ButterKnife.bind(this)

        DaggerLoginComponent.builder()
                .appComponent(App.getAppComponent())
                .build().inject(this)

        mLoginPresenter.attachView(this)

        setSignUpButton()
        setAgreementButton()
    }

    private fun setSignUpButton() {
        val signUpText = SpannableString(
                getString(R.string.don_t_have_an_account_sign_up)
        )

        val signUpClickable = object : ClickableSpan() {
            override fun onClick(textView: View) {
                Toast.makeText(
                        this@LoginActivity,
                        "Signing up is not available for now.",
                        Toast.LENGTH_SHORT).show()
            }
        }

        signUpText.setSpan(signUpClickable, 23, 30, 0)
        signUpText.setSpan(ForegroundColorSpan(
                ContextCompat.getColor(applicationContext, R.color.colorAccent)), 23, 30, 0)

        signUp.movementMethod = LinkMovementMethod.getInstance()
        signUp.setText(signUpText, TextView.BufferType.SPANNABLE)
        signUp.isSelected = true
    }

    private fun setAgreementButton() {
        val agreementText = SpannableString(
                getString(R.string.terms_of_service_and_privacy_policy)
        )

        val termsAndCondition = object : ClickableSpan() {
            override fun onClick(textView: View) {}
        }

        val privacy = object : ClickableSpan() {
            override fun onClick(textView: View) {}
        }

        agreementText.setSpan(termsAndCondition, 36, 52, 0)
        agreementText.setSpan(privacy, 57, 71, 0)
        agreementText.setSpan(ForegroundColorSpan(
                ContextCompat.getColor(applicationContext, R.color.colorWeak)), 23, 30, 0)
        agreementText.setSpan(ForegroundColorSpan(
                ContextCompat.getColor(applicationContext, R.color.colorWeak)), 23, 30, 0)
        agreementText.setSpan(UnderlineSpan(), 36, 52, 0)
        agreementText.setSpan(UnderlineSpan(), 57, 71, 0)

        agreement.movementMethod = LinkMovementMethod.getInstance()
        agreement.setText(agreementText, TextView.BufferType.SPANNABLE)
        agreement.isSelected = true
    }

    override fun onDestroy() {
        super.onDestroy()
        unbinder.unbind()
    }

    override fun onLoginSuccess() {
        startActivity(Intent(this, CallInfoActivity::class.java))
        finish()
    }

    override fun onLoginError() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Authentication error")
                .setCancelable(true)
                .setNeutralButton("Ok") { dialogInterface, i -> dialogInterface.dismiss() }
                .create()
                .show()
    }
}
