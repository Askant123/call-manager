package com.nextgoalapps.callmanager.view.login

import com.nextgoalapps.callmanager.view.BaseView

interface LoginView : BaseView {
    fun onLoginSuccess()
    fun onLoginError()
}