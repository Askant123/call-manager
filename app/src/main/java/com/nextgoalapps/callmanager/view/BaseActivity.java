package com.nextgoalapps.callmanager.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public abstract class BaseActivity extends AppCompatActivity implements BaseView{
    private ProgressDialog dialog;

    @Override
    public void showLoadProgress(String... message) {
        String text;
        if (message.length > 0) {
            text = message[0];
        } else {
            text = "Loading. Please wait...";
        }
        dialog = ProgressDialog.show(this, "",
                text, true);
    }

    @Override
    public void hideLoadProgress() {
        dialog.dismiss();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public Activity getViewActivity() {
        return this;
    }
}
