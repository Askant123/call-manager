package com.nextgoalapps.callmanager.view.settings.nested

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.nextgoalapps.callmanager.utils.DisabledCaller
import com.nextgoalapps.callmanager.utils.Utils

class DisabledCallsAdapter(private val list: List<String>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = convertView ?: View.inflate(parent?.context, android.R.layout.simple_list_item_1, null)
        val viewHolder = ViewHolder(view)
        viewHolder.name.text = list[position]
        viewHolder.name.setPadding(Utils.dpToPx(24, parent?.context),0,0,0)
        return view
    }

    override fun getItem(p0: Int): String = list[p0]

    override fun getItemId(p0: Int): Long = p0.toLong()

    override fun getCount(): Int = list.size

    class ViewHolder(view: View) {
        val name: TextView = view.findViewById(android.R.id.text1) as TextView
    }
}