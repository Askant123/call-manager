package com.nextgoalapps.callmanager.view.callInfo

import com.nextgoalapps.callmanager.view.BaseView


interface CallInfoView : BaseView {
    fun onProjectsLoaded()
    fun onProjectsLoadError()

    fun onCallSaved()
    fun onCallSaveError()
}