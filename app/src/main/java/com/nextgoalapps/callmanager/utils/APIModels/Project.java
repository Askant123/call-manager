package com.nextgoalapps.callmanager.utils.APIModels;


import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Project implements Comparable<Project> {

    @SerializedName("uuid")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Project buildFromString(String string) {
        Project project = new Project();
        try {
            JSONObject jsonObject = new JSONObject(string);
            project.id = jsonObject.getString("id");
            project.name = jsonObject.getString("name");
            project.description = jsonObject.getString("description");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return project;
    }

    public String toString() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("name", name);
            jsonObject.put("description", description);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    @Override
    public int compareTo(@NonNull Project project) {
        return name.compareTo(project.name);
    }
}
