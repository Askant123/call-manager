package com.nextgoalapps.callmanager.utils.APIModels;

import com.google.gson.annotations.SerializedName;

public class AccessToken {
    @SerializedName("accessToken")
    public String value;
}
