package com.nextgoalapps.callmanager.utils;

import com.nextgoalapps.callmanager.utils.APIModels.Project;

import java.util.ArrayList;

public class DataMockups {

    public static ArrayList<Project> getProjects() {
        ArrayList<Project> res = new ArrayList<>();
        Project project1 = new Project();
        project1.setId("11");
        project1.setName("Project 11");
        project1.setDescription("Description");
        Project project2 = new Project();
        project2.setId("22");
        project2.setName("Project 22");
        project2.setDescription("Description2");
        res.add(project1);
        res.add(project2);
        return res;
    }

}
