package com.nextgoalapps.callmanager.utils

data class DisabledCaller(val number: String, val name: String? = null){
    fun getPresentation(): String = name ?: number
}