package com.nextgoalapps.callmanager.utils;

import com.nextgoalapps.callmanager.utils.APIModels.AccessToken;
import com.nextgoalapps.callmanager.utils.APIModels.Call;
import com.nextgoalapps.callmanager.utils.APIModels.Project;
import com.nextgoalapps.callmanager.utils.APIModels.User;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIInterface {

    @POST("auth/sign-in")
    Observable<AccessToken> signIn(@Body User user);

    @GET("projects")
    Observable<List<Project>> getAllProjects(@Header("Authorization") String accessToken);

    @POST("projects/{projectUUID}/time")
    Observable<Response<ResponseBody>> addCallDurationToProject(@Body Call call,
                                                                @Path("projectUUID") String projectId,
                                                                @Header("Authorization") String accessToken);

}
