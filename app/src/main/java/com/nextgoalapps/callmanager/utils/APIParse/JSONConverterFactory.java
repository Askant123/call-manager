package com.nextgoalapps.callmanager.utils.APIParse;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

public class JSONConverterFactory extends Converter.Factory {
    private final Gson gson;
    private static final MediaType CONTENT_TYPE =
            MediaType.parse("application/json");

    public JSONConverterFactory(Gson gson) {
        this.gson = gson;
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(
            Type type, Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        for (Annotation annotation : parameterAnnotations) {
            if (annotation instanceof Root) {
                Root rootAnnotation = (Root) annotation;
                return new JSONRootConverter<>(gson, rootAnnotation.value());
            }
        }
        return null;
    }

    private final class JSONRootConverter<T> implements Converter<T, RequestBody> {
        private Gson gson;
        private String rootKey;

        private JSONRootConverter(Gson gson, String rootKey) {
            this.gson = gson;
            this.rootKey = rootKey;
        }

        @Override
        public RequestBody convert(T value) throws IOException {
            JsonElement element = gson.toJsonTree(value);
            JsonObject object = new JsonObject();
            object.add(this.rootKey, element);
            Log.d(element.toString(), object.toString());
            return RequestBody.create(CONTENT_TYPE, this.gson.toJson(object));
        }
    }
}
