package com.nextgoalapps.callmanager.utils;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;

import com.nextgoalapps.callmanager.receivers.PhoneStateBroadcastReceiver;

public class CallService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static boolean isActive;

    @Override
    public void onDestroy() {
        super.onDestroy();
        isActive = false;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isActive = true;

        PhoneStateBroadcastReceiver phoneStateBroadcastReceiver = new PhoneStateBroadcastReceiver();
        registerReceiver(phoneStateBroadcastReceiver, new IntentFilter(TelephonyManager.ACTION_PHONE_STATE_CHANGED));

        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Intent restartService = new Intent(getBaseContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getBaseContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager)getBaseContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() +1000, restartServicePI);
    }
}
