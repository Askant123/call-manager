package com.nextgoalapps.callmanager.utils.APIModels


import com.google.gson.annotations.SerializedName
import java.text.DecimalFormat

class Call {
    @SerializedName("time")
    var duration: Double = 0.toDouble()

    var number: String? = null

    var name: String? = null

    init {
        this.duration = -1.0
        this.number = ""
        this.name = ""
    }

    operator fun inc(): Call {
        if (duration > 0.toDouble()) {
            duration += 0.1
        }
        return this
    }

    operator fun dec(): Call {
        if (duration > 0.toDouble()) {
            duration -= 0.1
        }
        return this
    }
}
