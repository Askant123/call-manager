package com.nextgoalapps.callmanager.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.nextgoalapps.callmanager.utils.CallService;

public class DeviceLaunchReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, CallService.class);
            context.startService(serviceIntent);
        }
    }
}
