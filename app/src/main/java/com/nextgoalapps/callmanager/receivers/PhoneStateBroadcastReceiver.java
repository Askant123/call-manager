package com.nextgoalapps.callmanager.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.nextgoalapps.callmanager.view.callInfo.CallInfoActivity;

public class PhoneStateBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "PhoneStateBroadcastReceiver";
    Context mContext;
    static String incoming_nr;
    private static long startTime = -1;
    private static boolean callState = false;
    CustomPhoneStateListener customPhoneListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if(customPhoneListener == null) {
            customPhoneListener = new CustomPhoneStateListener();
        }
        telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE);
        mContext = context;
    }

    public class CustomPhoneStateListener extends PhoneStateListener {

        private static final String TAG = "CustomPhoneStateListener";

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            Log.e(TAG, String.valueOf(state));

            if(callState && startTime == -1 && (state == 2 || state == 0)){
                startTime = System.currentTimeMillis();
            }

            if(!callState){
                Log.d(TAG, "Call started");
                if(state == 2 || state == 0){
                    startTime = System.currentTimeMillis();
                }
                callState = true;
                incoming_nr = "";
            } else if (callState && incomingNumber != null && incomingNumber.length() > 0) {
                incoming_nr = incomingNumber;
                Log.d(TAG, "number" + incoming_nr);
            } else if (callState && !incoming_nr.equals("") && state != 2) {
                Log.d(TAG, "call ended");
                callState = false;
                long now = System.currentTimeMillis();
                long difference = now - startTime;

                startTime = -1;

                Intent intentone = new Intent(mContext.getApplicationContext(), CallInfoActivity.class);
                intentone.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                intentone.putExtra("Number", incoming_nr);
                intentone.putExtra("Duration", difference);
                mContext.startActivity(intentone);
            }

        }
    }
}