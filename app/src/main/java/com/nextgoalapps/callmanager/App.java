package com.nextgoalapps.callmanager;

import android.support.multidex.MultiDexApplication;

import com.nextgoalapps.callmanager.di.components.AppComponent;
import com.nextgoalapps.callmanager.di.components.DaggerAppComponent;
import com.nextgoalapps.callmanager.di.modules.AppContextModule;
import com.nextgoalapps.callmanager.di.modules.NetModule;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

public class App extends MultiDexApplication {

    private static AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        Fabric.with(this, new Crashlytics());

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.roboto_regular))
                .setFontAttrId(R.attr.fontPath)
                .build());

        mAppComponent = DaggerAppComponent.builder()
                .appContextModule(new AppContextModule(this))
                .netModule(new NetModule("http://h2721533.stratoserver.net:9001/api/v1/"))
                .build();
    }

    public static AppComponent getAppComponent() {
        return mAppComponent;
    }
}
