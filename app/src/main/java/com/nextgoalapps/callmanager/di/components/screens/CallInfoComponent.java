package com.nextgoalapps.callmanager.di.components.screens;

import com.nextgoalapps.callmanager.di.components.AppComponent;
import com.nextgoalapps.callmanager.di.modules.screens.CallInfoModule;
import com.nextgoalapps.callmanager.di.scopes.ActivityScope;
import com.nextgoalapps.callmanager.view.callInfo.CallInfoActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {CallInfoModule.class}, dependencies = {AppComponent.class})
public interface CallInfoComponent {
    void inject(CallInfoActivity activity);

}
