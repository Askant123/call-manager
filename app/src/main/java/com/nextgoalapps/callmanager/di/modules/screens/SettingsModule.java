package com.nextgoalapps.callmanager.di.modules.screens;

import com.nextgoalapps.callmanager.presenter.settings.SettingsPresenter;
import com.nextgoalapps.callmanager.presenter.settings.SettingsPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class SettingsModule {
    @Binds
    public abstract SettingsPresenter bindSettingsPresenter(SettingsPresenterImpl settingsPresenter);
}
