package com.nextgoalapps.callmanager.di.modules;

import com.nextgoalapps.callmanager.App;
import com.nextgoalapps.callmanager.di.scopes.AppScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AppContextModule {

    private App mApplication;

    public AppContextModule(App application) {
        mApplication = application;
    }

    @Provides
    @AppScope
    App providesApplication() {
        return mApplication;
    }
}