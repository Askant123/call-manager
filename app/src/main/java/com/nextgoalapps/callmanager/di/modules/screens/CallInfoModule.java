package com.nextgoalapps.callmanager.di.modules.screens;


import com.nextgoalapps.callmanager.presenter.callInfo.CallInfoPresenter;
import com.nextgoalapps.callmanager.presenter.callInfo.CallInfoPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class CallInfoModule {
    @Binds
    public abstract CallInfoPresenter bindCallInfoPresenter(CallInfoPresenterImpl callInfoPresenter);
}
