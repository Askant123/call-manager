package com.nextgoalapps.callmanager.di.components;

import com.nextgoalapps.callmanager.App;
import com.nextgoalapps.callmanager.di.modules.AppContextModule;
import com.nextgoalapps.callmanager.di.modules.ContactsModule;
import com.nextgoalapps.callmanager.di.modules.NetModule;
import com.nextgoalapps.callmanager.di.scopes.AppScope;
import com.nextgoalapps.callmanager.model.ContactsManager;
import com.nextgoalapps.callmanager.model.DataManager;
import com.nextgoalapps.callmanager.model.SharedPreferencesManager;

import dagger.Component;
import retrofit2.Retrofit;

@AppScope
@Component(modules = {AppContextModule.class, NetModule.class, ContactsModule.class})
public interface AppComponent {

    Retrofit retrofit();
    DataManager dataManager();
    SharedPreferencesManager sharedPreferencesManager();
    App context();
    ContactsManager contactsManager();

}
