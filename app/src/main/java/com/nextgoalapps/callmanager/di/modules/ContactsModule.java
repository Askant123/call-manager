package com.nextgoalapps.callmanager.di.modules;

import com.nextgoalapps.callmanager.App;
import com.nextgoalapps.callmanager.di.scopes.AppScope;
import com.nextgoalapps.callmanager.model.ContactsManager;

import dagger.Module;
import dagger.Provides;

@Module
public class ContactsModule {

    @Provides
    @AppScope
    ContactsManager providesContactManager(App app){
        return new ContactsManager(app);
    }
}
