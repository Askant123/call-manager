package com.nextgoalapps.callmanager.di.modules.screens;

import com.nextgoalapps.callmanager.presenter.login.LoginPresenter;
import com.nextgoalapps.callmanager.presenter.login.LoginPresenterImpl;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class LoginModule {
    @Binds
    public abstract LoginPresenter bindLoginPresenter(LoginPresenterImpl loginPresenter);
}
