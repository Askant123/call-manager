package com.nextgoalapps.callmanager.di.components.screens;

import com.nextgoalapps.callmanager.di.components.AppComponent;
import com.nextgoalapps.callmanager.di.modules.screens.LoginModule;
import com.nextgoalapps.callmanager.di.scopes.ActivityScope;
import com.nextgoalapps.callmanager.view.login.LoginActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {LoginModule.class},dependencies = {AppComponent.class})
public interface LoginComponent {
    void inject(LoginActivity activity);
}
