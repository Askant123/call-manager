package com.nextgoalapps.callmanager.di.components.screens;

import com.nextgoalapps.callmanager.di.components.AppComponent;
import com.nextgoalapps.callmanager.di.modules.screens.SettingsModule;
import com.nextgoalapps.callmanager.di.scopes.ActivityScope;
import com.nextgoalapps.callmanager.view.settings.SettingsActivity;

import dagger.Component;

@ActivityScope
@Component(modules = {SettingsModule.class}, dependencies = {AppComponent.class})
public interface SettingsComponent {
    void inject(SettingsActivity activity);
}
