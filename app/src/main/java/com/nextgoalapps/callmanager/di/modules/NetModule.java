package com.nextgoalapps.callmanager.di.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.nextgoalapps.callmanager.App;
import com.nextgoalapps.callmanager.model.DataManager;
import com.nextgoalapps.callmanager.model.SharedPreferencesManager;
import com.nextgoalapps.callmanager.di.scopes.AppScope;
import com.nextgoalapps.callmanager.utils.APIParse.JSONConverterFactory;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetModule {

    String mUrl;

    public NetModule(String url) {
        this.mUrl = url;
    }

    @Provides
    @AppScope
    SharedPreferencesManager providesSharedPreferencesManager(App application) {
        return new SharedPreferencesManager(application);
    }

    @Provides
    @AppScope
    Retrofit providesAPIClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder().create();

        return new Retrofit.Builder()
                .baseUrl(mUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(new JSONConverterFactory(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
    }

    @Provides
    @AppScope
    DataManager providesDataManager() {
        return new DataManager();
    }
}
