package com.nextgoalapps.callmanager.model;


public class DataManager {

    private String accessToken = null;

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }

    public String getAccessToken(){
        return accessToken;
    }
}
