package com.nextgoalapps.callmanager.model;


import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

public class ContactsManager {

    private static final String TAG = "ContactsManager";

    private Context mContext;

    public ContactsManager(Context context) {
        mContext = context;
    }

    public String getNameByNumber(String number) {
        if (!number.equals("")) {
            Log.e(TAG, "getNameByNumber: " + number);
            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

            String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};

            String contactName = "";
            Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    contactName = cursor.getString(0);
                }
                cursor.close();
            }

            return contactName;
        } else {
            return "";
        }
    }

}
