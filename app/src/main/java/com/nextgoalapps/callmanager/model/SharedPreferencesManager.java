package com.nextgoalapps.callmanager.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.nextgoalapps.callmanager.utils.APIModels.Project;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class SharedPreferencesManager {

    private SharedPreferences mSharedPreferences;

    private final static String TITLE = "Call App";

    public SharedPreferencesManager(Context context) {
        mSharedPreferences = context.getSharedPreferences(TITLE, Context.MODE_PRIVATE);
//        mSharedPreferences.edit().clear().commit();
    }

    private final static String ACCESS_TOKEN = "Access Token";

    public void saveAccessToken(String accessToken) {
        mSharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    public String getAccessToken() {
        return mSharedPreferences.getString(ACCESS_TOKEN, "");
    }

    private final static String PROJECTS = "Projects";

    public void saveProjects(List<Project> projectList) {
        Set<String> set = new LinkedHashSet<>();
        for (Project project :
                projectList) {
            set.add(project.toString());
        }
        mSharedPreferences.edit().putStringSet(PROJECTS, set).apply();
    }

    public List<Project> getProjects() {
        Set<String> set = mSharedPreferences.getStringSet(PROJECTS, new LinkedHashSet<>());
        List<Project> res = new ArrayList<>();
        for (String value :
                set) {
            res.add(Project.buildFromString(value));
        }
        return res;
    }

    private final static String DISABLED_CALLERS = "Disabled callers";

    public void addCaller(String number) {
        Set<String> set = mSharedPreferences.getStringSet(DISABLED_CALLERS, new LinkedHashSet<>());
        if (!set.contains(number)) set.add(number);
        mSharedPreferences.edit().remove(DISABLED_CALLERS).putStringSet(DISABLED_CALLERS, set).commit();
        Log.e("add caller", number);
    }

    public void removeCaller(String number) {
        Set<String> set = mSharedPreferences.getStringSet(DISABLED_CALLERS, new LinkedHashSet<>());
        if (set.contains(number)) set.remove(number);
        mSharedPreferences.edit().remove(DISABLED_CALLERS).putStringSet(DISABLED_CALLERS, set).apply();
    }

    public List<String> getDisabledCallers() {
        Set<String> set = mSharedPreferences.getStringSet(DISABLED_CALLERS, new LinkedHashSet<>());
        List<String> res = new ArrayList<>();
        res.addAll(set);
        return res;
    }

    private final static String IS_DISABLED_CALLERS_ACTIVE = "Is disabled callers active";

    public boolean getIsDisabledCallersActive() {
//        return mSharedPreferences.getBoolean(IS_DISABLED_CALLERS_ACTIVE, false);
        return true;
    }

    public void setIsDisabledCallersActive(boolean isDisabledCallersActive) {
        mSharedPreferences.edit().putBoolean(IS_DISABLED_CALLERS_ACTIVE, isDisabledCallersActive).apply();
    }
}
