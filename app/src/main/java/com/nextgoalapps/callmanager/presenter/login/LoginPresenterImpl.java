package com.nextgoalapps.callmanager.presenter.login;

import android.content.Intent;

import com.nextgoalapps.callmanager.App;
import com.nextgoalapps.callmanager.model.DataManager;
import com.nextgoalapps.callmanager.model.SharedPreferencesManager;
import com.nextgoalapps.callmanager.utils.APIInterface;
import com.nextgoalapps.callmanager.utils.APIModels.User;
import com.nextgoalapps.callmanager.view.BaseView;
import com.nextgoalapps.callmanager.view.callInfo.CallInfoActivity;
import com.nextgoalapps.callmanager.view.login.LoginView;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginView mView;

    private APIInterface mAPIInterface;
    private SharedPreferencesManager mSharedPreferencesManager;
    private DataManager mDataManager;

    @Inject
    public LoginPresenterImpl(Retrofit retrofit,
                              SharedPreferencesManager sharedPreferencesManager,
                              DataManager dataManager, App context) {
        mAPIInterface = retrofit.create(APIInterface.class);
        mSharedPreferencesManager = sharedPreferencesManager;
        if (!mSharedPreferencesManager.getAccessToken().equals("")) {
            Intent intent = new Intent(context, CallInfoActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
        mDataManager = dataManager;
    }

    @Override
    public void login(String login, String password) {
        mView.showLoadProgress("Logging in...");
        mAPIInterface.signIn(new User(login, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(accessToken -> {
                    mSharedPreferencesManager.saveAccessToken(accessToken.value);
                    mDataManager.setAccessToken(accessToken.value);
                    mView.hideLoadProgress();
                    mView.onLoginSuccess();
                }, throwable -> {
                    mView.hideLoadProgress();
                    mView.onLoginError();
                });
    }

    @Override
    public void attachView(BaseView view) {
        mView = (LoginView) view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
