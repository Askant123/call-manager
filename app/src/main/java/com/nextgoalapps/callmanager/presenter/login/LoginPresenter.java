package com.nextgoalapps.callmanager.presenter.login;


import com.nextgoalapps.callmanager.presenter.BasePresenter;

public interface LoginPresenter extends BasePresenter{
    void login(String login, String password);
}
