package com.nextgoalapps.callmanager.presenter.settings

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.nextgoalapps.callmanager.model.ContactsManager
import com.nextgoalapps.callmanager.model.SharedPreferencesManager
import com.nextgoalapps.callmanager.view.BaseView
import com.nextgoalapps.callmanager.view.settings.SettingsView
import javax.inject.Inject


class SettingsPresenterImpl @Inject
constructor(sharedPreferencesManager: SharedPreferencesManager, contactsManager: ContactsManager) : SettingsPresenter {
    override fun removeCaller(id: Int) {
        mSharedPreferencesManager.removeCaller(callers[id])
    }

    private lateinit var callers: List<String>

    override fun getDisabledCallers(): List<String> {
        callers = mSharedPreferencesManager.disabledCallers
        val myVersion = Build.VERSION.SDK_INT
        return if (myVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHaveContactsPermission()) {
                requestForContactsPermission()
                ArrayList()
            } else {
                callListToPresentation(callers)
            }
        } else {
            callListToPresentation(callers)
        }
    }

    override fun setIsDisabledCallersActive(isActive: Boolean) {
        mSharedPreferencesManager.isDisabledCallersActive = isActive
    }

    override fun getIsDisabledCallersActive(): Boolean = mSharedPreferencesManager.isDisabledCallersActive

    private fun callListToPresentation(callers: List<String>): List<String> {
        return callers.map { value ->
            Log.e("Value", value)
            val result = mContactsManager.getNameByNumber(value)
            if (result == "") {
                value
            } else {
                result
            }
        }
    }

    private var mView: SettingsView? = null

    private var mSharedPreferencesManager: SharedPreferencesManager = sharedPreferencesManager
    private var mContactsManager: ContactsManager = contactsManager

    private fun checkIfAlreadyHaveContactsPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(mView!!.viewActivity!!, Manifest.permission.READ_CONTACTS)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestForContactsPermission() {
        ActivityCompat.requestPermissions(mView!!.viewActivity, arrayOf(Manifest.permission.READ_CONTACTS), 102)
    }

    override fun attachView(view: BaseView) {
        mView = view as SettingsView
    }

    override fun detachView() {
        mView = null
    }
}
