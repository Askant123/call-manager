package com.nextgoalapps.callmanager.presenter.settings

import com.nextgoalapps.callmanager.presenter.BasePresenter

interface SettingsPresenter: BasePresenter {
    fun removeCaller(id: Int)

    fun getDisabledCallers(): List<String>

    fun setIsDisabledCallersActive(isActive: Boolean)
    fun getIsDisabledCallersActive(): Boolean
}