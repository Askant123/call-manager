package com.nextgoalapps.callmanager.presenter.callInfo

import com.nextgoalapps.callmanager.presenter.BasePresenter
import java.util.ArrayList

interface CallInfoPresenter : BasePresenter {

    val projectsPresentation: ArrayList<String>

    val allProjects: ArrayList<String>

    val numberPresentation: String
    fun init()

    fun reset()

    fun startService()

    fun search(key: String)

    fun setNumber(number: String)

    fun saveCallDuration(projectNumberInTheList: Int)

    fun updateProjects()

    fun getCallDuration(vararg durationInMills: Long): Double

    fun addDisabledCaller()
    fun isCallerDisabled(): Boolean

    fun decrementDuration()
    fun incrementDuration()
}
