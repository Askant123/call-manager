package com.nextgoalapps.callmanager.presenter.callInfo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import com.nextgoalapps.callmanager.model.ContactsManager
import com.nextgoalapps.callmanager.model.DataManager
import com.nextgoalapps.callmanager.model.SharedPreferencesManager
import com.nextgoalapps.callmanager.utils.APIInterface
import com.nextgoalapps.callmanager.utils.APIModels.Call
import com.nextgoalapps.callmanager.utils.APIModels.Project
import com.nextgoalapps.callmanager.utils.CallService
import com.nextgoalapps.callmanager.view.BaseView
import com.nextgoalapps.callmanager.view.callInfo.CallInfoView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject

class CallInfoPresenterImpl @Inject
constructor(retrofit: Retrofit,
            private val mSharedPreferencesManager: SharedPreferencesManager, private val mDataManager: DataManager,
            private val mContactsManager: ContactsManager) : CallInfoPresenter {

    private var mView: CallInfoView? = null

    private val mAPIInterface: APIInterface = retrofit.create(APIInterface::class.java)

    private var projects: List<Project>? = null

    override val projectsPresentation: ArrayList<String>
        get() {
            val res = ArrayList<String>()
            Observable.fromIterable(if (searchProjects.size == 0) projects else searchProjects)
                    .map<String>({ it.name })
                    .subscribe({ res.add(it) })
            return res
        }

    override lateinit var allProjects: ArrayList<String>
    private var searchProjects = ArrayList<Project>()

    override val numberPresentation: String
        get() {
            call!!.name = getContactName(call!!.number)
            return call!!.number + if (call!!.name == "") "" else " (" + call!!.name + ")"
        }

    private var call: Call? = null

    private fun checkIfAlreadyHavePhoneStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(mView!!.viewActivity, Manifest.permission.READ_PHONE_STATE)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestForPhoneStatePermission() {
        ActivityCompat.requestPermissions(mView!!.viewActivity, arrayOf(Manifest.permission.READ_PHONE_STATE), 101)
    }

    override fun init() {
        reset()
        if (!CallService.isActive) {
            projects = ArrayList()
            val activity = mView!!.viewActivity
            val myVersion = Build.VERSION.SDK_INT
            if (myVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
                if (!checkIfAlreadyHavePhoneStatePermission()) {
                    requestForPhoneStatePermission()
                } else {
                    activity.startService(Intent(activity.applicationContext, CallService::class.java))
                    loadProjectsFromAPI()
                }
            }
        } else {
            projects = mSharedPreferencesManager.projects
            Collections.sort(projects!!) { obj, project -> obj.compareTo(project) }
            mView!!.onProjectsLoaded()
        }
    }

    override fun reset() {
        call = Call()
    }

    private fun loadProjectsFromAPI() {
        mView!!.showLoadProgress("Loading projects...")
        val accessToken: String = if (mDataManager.accessToken == null) {
            mSharedPreferencesManager.accessToken
        } else {
            mDataManager.accessToken
        }
        mAPIInterface.getAllProjects(accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ allProjects ->
                    projects = allProjects
                    Collections.sort(projects!!) { obj, project -> obj.compareTo(project) }
                    mSharedPreferencesManager.saveProjects(allProjects)
                    mView!!.onProjectsLoaded()
                    mView!!.hideLoadProgress()
                }) {
                    mView!!.hideLoadProgress()
                    mView!!.onProjectsLoadError()
                }
    }

    override fun startService() {
        val activity = mView!!.viewActivity
        activity.startService(Intent(activity.applicationContext, CallService::class.java))
    }

    override fun search(key: String) {
        searchProjects = ArrayList()
        Observable.fromIterable(projects!!)
                .filter { element -> element.name.toLowerCase().contains(key.toLowerCase()) }
                .subscribe({ searchProjects.add(it) })
    }

    override fun setNumber(number: String) {
        call!!.number = number
    }

    override fun saveCallDuration(projectNumberInTheList: Int) {
        mView!!.showLoadProgress("Saving the time...")
        val accessToken: String = if (mDataManager.accessToken == null) {
            mSharedPreferencesManager.accessToken
        } else {
            mDataManager.accessToken
        }
        mAPIInterface.addCallDurationToProject(call!!,
                projects!![projectNumberInTheList].id,
                accessToken)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ o ->
                    mView!!.hideLoadProgress()
                    when (o.code()) {
                        204 -> mView!!.onCallSaved()
                        else -> mView!!.onCallSaveError()
                    }
                }) {
                    mView!!.hideLoadProgress()
                    mView!!.onCallSaveError()
                }
    }

    override fun updateProjects() {
        loadProjectsFromAPI()
    }

    override fun getCallDuration(vararg durationInMills: Long): Double {
        val callDuration: Double = if (durationInMills.isNotEmpty()) {
            val res = Math.ceil((durationInMills[0].toFloat() / 60f / 60f / 100f).toDouble())
            val finalRes = res / 10.0
            finalRes
        } else {
            call?.duration ?: -1.0
        }
        Log.e("Call duration", callDuration.toString())
        call!!.duration = callDuration
        Log.e("Call duration", call!!.duration.toString())
        Log.e("Call duration", callDuration.toString())
        return callDuration
    }

    private fun checkIfAlreadyHaveContactsPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(mView!!.viewActivity, Manifest.permission.READ_CONTACTS)
        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestForContactsPermission() {
        ActivityCompat.requestPermissions(mView!!.viewActivity, arrayOf(Manifest.permission.READ_CONTACTS), 102)
    }

    private fun getContactName(number: String?): String {
        val myVersion = Build.VERSION.SDK_INT
        return if (myVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHaveContactsPermission()) {
                requestForContactsPermission()
                ""
            } else {
                mContactsManager.getNameByNumber(number)
            }
        } else {
            mContactsManager.getNameByNumber(number)
        }
    }

    override fun addDisabledCaller() {
        mSharedPreferencesManager.addCaller(call!!.number)
    }

    override fun isCallerDisabled(): Boolean = mSharedPreferencesManager.isDisabledCallersActive and
            mSharedPreferencesManager.disabledCallers.contains(call?.number)

    override fun decrementDuration() {
        call?.dec()
    }

    override fun incrementDuration() {
        call?.inc()
        Log.e("Call",call?.duration.toString())
    }

    override fun attachView(view: BaseView) {
        mView = view as CallInfoView
    }

    override fun detachView() {
        mView = null
    }
}
