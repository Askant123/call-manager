package com.nextgoalapps.callmanager.presenter;


import com.nextgoalapps.callmanager.view.BaseView;

public interface BasePresenter {
    void attachView(BaseView view);
    void detachView();
}
